package com.example.mobileconsulting.viewholderexample.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.mobileconsulting.viewholderexample.Adapter.SeasonAdapter;
import com.example.mobileconsulting.viewholderexample.Fragment.DetailFragment;
import com.example.mobileconsulting.viewholderexample.Interface.INavigationFragment;
import com.example.mobileconsulting.viewholderexample.Interface.IRowClicked;
import com.example.mobileconsulting.viewholderexample.Model.SeasonModel;
import com.example.mobileconsulting.viewholderexample.R;

import java.util.ArrayList;

public class FirstActivity extends AppCompatActivity implements View.OnClickListener, IRowClicked,
        INavigationFragment{

    private RecyclerView rvShows;
    private ArrayList<SeasonModel> seasons;

    private SeasonAdapter adapter;

    private boolean isLargeScreen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (findViewById(R.id.season_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            isLargeScreen = true;
        }


        View recyclerView = findViewById(R.id.rvShows);
        assert recyclerView != null;


        this.createAdapter((RecyclerView) recyclerView);
    }

    private void createAdapter(@NonNull RecyclerView recyclerView){

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        seasons = new ArrayList<>();
        seasons.clear();

        seasons.add(new SeasonModel("Arrow", "4 x 13 Sins and Father"
                , "in 2 days", "The CW / Mon 9:00pm"));
        seasons.add(new SeasonModel("Big Bang", "4 x 13 Sins and Father"
                , "in 2 days", "The CW / Tue 9:00pm"));
        seasons.add(new SeasonModel("Friends", "4 x 13 Sins and Father"
                , "in 2 days", "The CW / Wed 9:00pm"));
        seasons.add(new SeasonModel("Supergirl", "4 x 13 Sins and Father"
                , "in 2 days", "The CW / Thu 9:00pm"));
        seasons.add(new SeasonModel("Flash", "4 x 13 Sins and Father"
                , "in 2 days", "The CW / Fri 9:00pm"));
        seasons.add(new SeasonModel("DC Legends of tomoroow", "4 x 13 Sins and Father"
                , "in 2 days", "The CW / Sat 9:00pm"));


        adapter = new SeasonAdapter(FirstActivity.this, seasons, this);

        recyclerView.setAdapter(adapter);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_first, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this,FactoryActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /* Callback of clicked event in the recycler view */

    @Override
    public void callbackClick(int position) {

        if (isLargeScreen){
            DetailFragment fragment = DetailFragment.newInstance(seasons.get(position),this);

            if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                callbackReplaceLast(fragment);
            else
                showFragment(fragment);
        }else{
            Intent intent = new Intent(FirstActivity.this, DetailActivity.class);
            intent.putExtra("title", seasons.get(position).getTitle());
            startActivity(intent);
        }
    }

    /* Callback for comunication between Activity and Fragment*/


    @Override
    public void onBackPressed() {
        customBack();
    }

    @Override
    public void callbackPushFragment(Fragment fragment) {
        showFragment(fragment);
    }

    @Override
    public void callbackPop() {
        pop();
    }

    @Override
    public void callbackReplaceLast(Fragment fragment) {
        pop();
        showFragment(fragment);
    }

    private void pop(){
        getSupportFragmentManager().popBackStack();
    }

    private void customBack(){
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 1 || count == 0){
            finish();
        }
        else{
            pop();
        }
    }

    private void showFragment(Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        ft.setCustomAnimations(R.anim.enter_anim, R.anim.exit_anim, R.anim.enter_anim, R.anim.exit_anim);
        ft.replace(R.id.season_detail_container, fragment);
        ft.addToBackStack(fragment.getClass().getName());
        ft.commit();
    }
}
