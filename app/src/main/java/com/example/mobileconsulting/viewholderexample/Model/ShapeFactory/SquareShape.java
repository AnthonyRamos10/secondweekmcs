package com.example.mobileconsulting.viewholderexample.Model.ShapeFactory;

import android.content.Context;
import android.widget.Toast;

import com.example.mobileconsulting.viewholderexample.Interface.IShapeFactory;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public class SquareShape implements IShapeFactory {
    @Override
    public void draw(Context context) {
        Toast.makeText(context, "Insert Square shape : draw() method", Toast.LENGTH_SHORT).show();
        System.out.println("Insert Square shape: draw() method");
    }
}
