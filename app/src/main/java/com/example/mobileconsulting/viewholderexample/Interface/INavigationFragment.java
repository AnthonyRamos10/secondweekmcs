package com.example.mobileconsulting.viewholderexample.Interface;

import android.support.v4.app.Fragment;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public interface INavigationFragment {
    void callbackPushFragment(Fragment fragment);
    void callbackPop();
    void callbackReplaceLast(Fragment fragment);
}
