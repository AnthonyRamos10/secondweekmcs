package com.example.mobileconsulting.viewholderexample.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mobileconsulting.viewholderexample.Interface.IRowClicked;
import com.example.mobileconsulting.viewholderexample.Model.SeasonModel;
import com.example.mobileconsulting.viewholderexample.R;
import com.example.mobileconsulting.viewholderexample.ViewHolder.SeasonViewHolder;

import java.util.ArrayList;

/**
 * Created by mobileconsulting on 4/30/18.
 */

public class SeasonAdapter extends RecyclerView.Adapter<SeasonViewHolder> {

    private ArrayList<SeasonModel> seasons;
    private Context context;
    private IRowClicked iRowClicked;

    public SeasonAdapter(Context context, ArrayList<SeasonModel> seasons, IRowClicked iRowClicked){
        this.context = context;
        this.seasons = seasons;
        this.iRowClicked = iRowClicked;
    }

    @Override
    public SeasonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_season_layout, parent, false);
        SeasonViewHolder seasonViewHolder = new SeasonViewHolder(v,iRowClicked);
        return seasonViewHolder;
    }

    @Override
    public void onBindViewHolder(SeasonViewHolder holder, int position) {
        holder.bind(context,seasons.get(position));
    }

    @Override
    public int getItemCount() {
        return seasons.size();
    }
}
