package com.example.mobileconsulting.viewholderexample.Factory.Producer;

import com.example.mobileconsulting.viewholderexample.Abstract.AbstractFactory;
import com.example.mobileconsulting.viewholderexample.Factory.ColorFactory;
import com.example.mobileconsulting.viewholderexample.Factory.ShapeFactory;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public class FactoryProducer {

    public static AbstractFactory getFactory(String choice){
        if(choice.equalsIgnoreCase("SHAPE")){
            return new ShapeFactory();

        }else if(choice.equalsIgnoreCase("COLOR")){
            return new ColorFactory();
        }

        return null;
    }
}
