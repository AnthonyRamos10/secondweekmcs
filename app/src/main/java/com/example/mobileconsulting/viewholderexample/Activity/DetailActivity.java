package com.example.mobileconsulting.viewholderexample.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.mobileconsulting.viewholderexample.R;

public class DetailActivity extends AppCompatActivity {

    private TextView tvTitle;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);



        extras = getIntent().getExtras();

        tvTitle = findViewById(R.id.tvTitle);

        tvTitle.setText(extras.getString("title"));

    }
}
