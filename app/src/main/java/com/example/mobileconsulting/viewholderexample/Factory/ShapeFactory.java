package com.example.mobileconsulting.viewholderexample.Factory;

import com.example.mobileconsulting.viewholderexample.Abstract.AbstractFactory;
import com.example.mobileconsulting.viewholderexample.Interface.IColorFactory;
import com.example.mobileconsulting.viewholderexample.Interface.IShapeFactory;
import com.example.mobileconsulting.viewholderexample.Model.ShapeFactory.CircleShape;
import com.example.mobileconsulting.viewholderexample.Model.ShapeFactory.SquareShape;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public class ShapeFactory extends AbstractFactory {

    @Override
    public IShapeFactory getShape(String shape){

        if(shape == null){
            return null;
        }

        if(shape.equalsIgnoreCase("CIRCLE")){
            return new CircleShape();

        }else if(shape.equalsIgnoreCase("SQUARE")){
            return new SquareShape();
        }

        return null;
    }

    @Override
    public IColorFactory getColor(String color) {
        return null;
    }
}
