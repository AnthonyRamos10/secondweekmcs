package com.example.mobileconsulting.viewholderexample.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mobileconsulting.viewholderexample.Interface.INavigationFragment;
import com.example.mobileconsulting.viewholderexample.Model.SeasonModel;
import com.example.mobileconsulting.viewholderexample.R;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public class DetailFragment extends Fragment{

    private SeasonModel season;

    private INavigationFragment interfaceNavigation;

    public void setSeason(SeasonModel season) {
        this.season = season;
    }

    public void setInterfaceNavigation(INavigationFragment interfaceNavigation) {
        this.interfaceNavigation = interfaceNavigation;
    }

    public DetailFragment(){}

    public static DetailFragment newInstance(SeasonModel season, INavigationFragment interfaceNavigation){
        DetailFragment fragment = new DetailFragment();
        fragment.setSeason(season);
        fragment.setInterfaceNavigation(interfaceNavigation);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_detail, container, false);

        TextView tvTitle = view.findViewById(R.id.tvTitle);

        if (season != null)
            tvTitle.setText(season.getTitle());

        return view;
    }
}
