package com.example.mobileconsulting.viewholderexample.Interface;

/**
 * Created by mobileconsulting on 4/30/18.
 */

public interface IRowClicked {

    void callbackClick(int position);
}
