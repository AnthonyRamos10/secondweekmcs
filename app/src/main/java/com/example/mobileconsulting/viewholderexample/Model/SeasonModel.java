package com.example.mobileconsulting.viewholderexample.Model;

/**
 * Created by mobileconsulting on 4/30/18.
 */

public class SeasonModel {

    public SeasonModel(String title, String subtitle, String status, String sDate) {
        this.title = title;
        this.subtitle = subtitle;
        this.status = status;
        this.sDate = sDate;
    }

    private String title;
    private String subtitle;
    private String status;
    private String sDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }
}
