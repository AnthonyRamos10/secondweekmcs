package com.example.mobileconsulting.viewholderexample.Abstract;

import com.example.mobileconsulting.viewholderexample.Interface.IColorFactory;
import com.example.mobileconsulting.viewholderexample.Interface.IShapeFactory;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public abstract class AbstractFactory {
    public abstract IColorFactory getColor(String color);
    public abstract IShapeFactory getShape(String shape);
}
