package com.example.mobileconsulting.viewholderexample.Interface;

import android.content.Context;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public interface IColorFactory {
    void fill(Context context);
}
