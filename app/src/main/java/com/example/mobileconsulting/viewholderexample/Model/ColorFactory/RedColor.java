package com.example.mobileconsulting.viewholderexample.Model.ColorFactory;

import android.content.Context;
import android.widget.Toast;

import com.example.mobileconsulting.viewholderexample.Interface.IColorFactory;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public class RedColor implements IColorFactory {
    @Override
    public void fill(Context context) {
        Toast.makeText(context, "Insert Red color: fill() method", Toast.LENGTH_SHORT).show();
        System.out.println("Insert Red color: fill() method");
    }
}
