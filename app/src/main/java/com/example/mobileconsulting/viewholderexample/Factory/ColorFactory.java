package com.example.mobileconsulting.viewholderexample.Factory;

import com.example.mobileconsulting.viewholderexample.Abstract.AbstractFactory;
import com.example.mobileconsulting.viewholderexample.Interface.IColorFactory;
import com.example.mobileconsulting.viewholderexample.Interface.IShapeFactory;
import com.example.mobileconsulting.viewholderexample.Model.ColorFactory.BlueColor;
import com.example.mobileconsulting.viewholderexample.Model.ColorFactory.RedColor;

/**
 * Created by mobileconsulting on 5/1/18.
 */

public class ColorFactory extends AbstractFactory {
    @Override
    public IColorFactory getColor(String color) {

        if(color == null){
            return null;
        }

        if(color.equalsIgnoreCase("RED")){
            return new RedColor();

        }else if(color.equalsIgnoreCase("BLUE")){
            return new BlueColor();
        }

        return null;
    }

    @Override
    public IShapeFactory getShape(String shape) {
        return null;
    }
}
