package com.example.mobileconsulting.viewholderexample.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mobileconsulting.viewholderexample.Abstract.AbstractFactory;
import com.example.mobileconsulting.viewholderexample.Factory.Producer.FactoryProducer;
import com.example.mobileconsulting.viewholderexample.Interface.IColorFactory;
import com.example.mobileconsulting.viewholderexample.Interface.IShapeFactory;
import com.example.mobileconsulting.viewholderexample.R;

public class FactoryActivity extends AppCompatActivity {

    private AbstractFactory abstractFactory;

    private Spinner spMain, spSub;

    private boolean isColor = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factory);

        spMain = findViewById(R.id.spMain);
        spSub = findViewById(R.id.spSub);

        spMain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                    spSub.setFocusable(true);

                if (position == 1){

                    isColor = false;

                    abstractFactory = FactoryProducer.getFactory("SHAPE");

                    String[] array = getResources().getStringArray(R.array.ar_spin_shape);

                    spSub.setAdapter(new ArrayAdapter<String>(FactoryActivity.this,   android.R.layout.simple_spinner_item, array));
                }

                if (position == 2){

                    isColor = true;

                    abstractFactory = FactoryProducer.getFactory("COLOR");

                    String[] array = getResources().getStringArray(R.array.ar_spin_color);

                    spSub.setAdapter(new ArrayAdapter<String>(FactoryActivity.this,   android.R.layout.simple_spinner_item, array));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position > 0){
                    if (isColor){

                        IColorFactory color;

                        if (position == 1)
                            color = abstractFactory.getColor("RED");
                        else
                            color = abstractFactory.getColor("BLUE");

                        color.fill(FactoryActivity.this);

                    }else{

                        IShapeFactory shape;

                        if (position == 1)
                            shape = abstractFactory.getShape("SQUARE");
                        else
                            shape = abstractFactory.getShape("CIRCLE");

                        shape.draw(FactoryActivity.this);

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
}
