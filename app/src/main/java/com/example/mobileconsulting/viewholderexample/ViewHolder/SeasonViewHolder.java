package com.example.mobileconsulting.viewholderexample.ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.mobileconsulting.viewholderexample.Interface.IRowClicked;
import com.example.mobileconsulting.viewholderexample.Model.SeasonModel;
import com.example.mobileconsulting.viewholderexample.R;

/**
 * Created by mobileconsulting on 4/30/18.
 */

public class SeasonViewHolder extends RecyclerView.ViewHolder {

    View itemView;

    TextView tvTitle;
    TextView tvSubtitle;
    TextView tvStatus;
    TextView tvDate;
    IRowClicked iRowClicked;


    public SeasonViewHolder(View itemView, IRowClicked iRowClicked) {
        super(itemView);

        this.itemView = itemView;

        this.tvTitle = itemView.findViewById(R.id.tvTitle);
        this.tvSubtitle = itemView.findViewById(R.id.tvSubtitle);
        this.tvStatus = itemView.findViewById(R.id.tvStatus);
        this.tvDate = itemView.findViewById(R.id.tvDate);

        this.iRowClicked = iRowClicked;
    }

    public void bind(Context context, SeasonModel season){

        tvTitle.setText(season.getTitle());
        tvSubtitle.setText(season.getSubtitle());
        tvStatus.setText(season.getStatus());
        tvDate.setText(season.getsDate());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iRowClicked.callbackClick(getAdapterPosition());
            }
        });

    }
}
